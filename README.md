# Produkter.dk React App

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `Framework`

- React Hooks

### `Component library`

- MUI (Material UI)

### `Main Components`

- ProductOverview
- FreeTextSearch
- CheckboxFilter

### `Completed tasks`

- Product Overview
- Tried checkbox but it is not working completely.
