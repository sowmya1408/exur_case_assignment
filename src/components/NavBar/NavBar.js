import React from "react";
import { AppBar, Toolbar, Typography } from "@mui/material";

const NavBar = () => {
  return (
    <AppBar position="relative" color="transparent">
      <Toolbar>
        <Typography variant="h4" color="primary" fontWeight="bold">
          Produkter.dk
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default NavBar;
