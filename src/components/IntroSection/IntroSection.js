import React from "react";
import { Container, Typography } from "@mui/material";
import useStyles from "./IntroSectionStyles";

const IntroSection = () => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <Container maxWidth="lg">
        <Typography variant="h2" color="primary" fontWeight="bold" gutterBottom>
          Se farlige og mangelfulde produkter
        </Typography>
        <Typography variant="h5" align="left" color="textSecondary" paragraph>
          Når myndigheder i Danmark og resten af EU opdager et farligt eller et
          mangelfuldt produkt, offentliggør danske myndigheder information om de
          farlige og mangelfulde produkter.
        </Typography>
        <Typography variant="h5" align="left" color="textSecondary" paragraph>
          Alvorligheden af fejl og mangler afgør om et produkt er farligt eller
          mangelfuldt. Du kan komme alvorligt til skade på et farligt produkt.
          Et mangelfuldt produkt har mindre fejl på produktet eller i
          dokumentationen, men det er ikke farligt at bruge.
        </Typography>
      </Container>
    </div>
  );
};

export default IntroSection;
