import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(() => ({
  container: {
    padding: "8px 0 6px",
  },
  button: {
    marginBottom: "40px",
  },
}));

export default useStyles;
