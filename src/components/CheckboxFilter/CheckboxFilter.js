import React from "react";
import {
  Grid,
  FormControl,
  FormLabel,
  FormGroup,
  FormControlLabel,
  Checkbox,
} from "@mui/material";
import ArrowForwardTwoToneIcon from "@mui/icons-material/ArrowForwardTwoTone";
const CheckboxFilter = ({
  checkedProducttype,
  changeTypeChecked,
  checkboxTerm,
  checkedProducttype2,
  setCheckedProducttype2,
  setChangeType2Text,
}) => {
  console.log(checkedProducttype);
  return (
    <Grid item xs={12} md={12}>
      <FormControl
        sx={{ m: 2, width: 400 }}
        component="fieldset"
        variant="standard"
      >
        <FormLabel component="legend">
          <ArrowForwardTwoToneIcon /> Filtrer på produkttype
        </FormLabel>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                checked={checkedProducttype}
                onChange={(e) => {
                  changeTypeChecked((prevState) => !prevState);
                  checkboxTerm(e.currentTarget.name);
                }}
                name="Farlige produkter"
              />
            }
            label="Farlige produkter"
          />
          <FormControlLabel
            control={
              <Checkbox
                checked={checkedProducttype2}
                onChange={(e) => {
                  setCheckedProducttype2((prevState) => !prevState);
                  setChangeType2Text(e.currentTarget.name);
                }}
                name="Mangelfulde produkter"
              />
            }
            label="Mangelfulde produkter"
          />
        </FormGroup>
      </FormControl>
    </Grid>
  );
};

export default CheckboxFilter;
