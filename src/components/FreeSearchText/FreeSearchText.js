import React from "react";
import { Grid, Typography, Paper, InputBase, IconButton } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";

const FreeSearchText = ({ searchTerm, handleTerm }) => {
  //console.log(searchTerm);
  return (
    <Grid item xs={12} md={4}>
      <Typography>Forside Se farlige og mangelfulde produkter</Typography>
      <Paper
        component="form"
        sx={{
          p: "2px 4px",
          display: "flex",
          alignItems: "center",
          width: 300,
        }}
      >
        <InputBase
          sx={{ ml: 1, flex: 1 }}
          placeholder="Søg produkter"
          inputProps={{ "aria-label": "Søg produkter" }}
          onChange={handleTerm}
        />
        <IconButton type="submit" sx={{ p: "10px" }} aria-label="search">
          <SearchIcon />
        </IconButton>
      </Paper>
    </Grid>
  );
};

export default FreeSearchText;
