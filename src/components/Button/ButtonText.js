import React from "react";
import { Container, Button } from "@mui/material";
import buttonStyles from "./ButtonTextStyle";

const ButtonText = () => {
  const classes = buttonStyles();
  return (
    <div>
      <Container className={classes.button} maxWidth="lg">
        <Button variant="outlined" color="primary">
          some text over here and looks good
        </Button>
      </Container>
    </div>
  );
};

export default ButtonText;
