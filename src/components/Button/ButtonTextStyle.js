import { makeStyles } from "@mui/styles";

const buttonStyles = makeStyles(() => ({
  button: {
    marginBottom: "40px",
  },
}));

export default buttonStyles;
