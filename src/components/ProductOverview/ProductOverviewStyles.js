import { makeStyles } from "@mui/styles";

const cardStyles = makeStyles(() => ({
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardHeader: {
    backgroundColor: "#19B5FE",
    width: "250px",
    height: "80px",
    color: "#fff",
  },
  cardMedia: {
    paddingTop: "56.25%",
  },
  cardContent: {
    flexGrow: 1,
  },
}));

export default cardStyles;
