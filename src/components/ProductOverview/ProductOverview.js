import React from "react";
import {
  Grid,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  Typography,
  CircularProgress,
  Box,
} from "@mui/material";
import cardStyles from "./ProductOverviewStyles";

const ProductOverview = ({ products, errors, load }) => {
  const classes = cardStyles();
  console.log(products);
  return (
    <Grid container item xs={12} md={8} spacing={3}>
      {load ? (
        <CircularProgress />
      ) : products ? (
        products.map((product) => {
          return (
            <Grid key={product.id} item xs={6} md={4}>
              <Card className={classes.card}>
                <CardHeader
                  className={classes.cardHeader}
                  title={
                    product.complianceType.id === 2
                      ? "Farlige produkter"
                      : "Mangelfulde produkter"
                  }
                />
                <CardMedia
                  className={classes.cardMedia}
                  image="https://source.unsplash.com/random"
                  title="product-images"
                />
                <CardContent className={classes.cardContent}>
                  <Typography variant="h6" color="primary" gutterBottom>
                    {product && product.name}
                  </Typography>
                  <Typography variant="body2" gutterBottom>
                    Kategori:{" "}
                    {
                      product.productCategoryRelations[0].productCategoryType
                        .name
                    }
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          );
        })
      ) : (
        <h2>{errors}</h2>
      )}
    </Grid>
  );
};

export default ProductOverview;
