import React, { useEffect, useState } from "react";
import "./App.css";
import { CssBaseline, Grid, Container } from "@mui/material";
import NavBar from "./components/NavBar/NavBar";
import IntroSection from "./components/IntroSection/IntroSection";
import ButtonText from "./components/Button/ButtonText";
import FreeSearchText from "./components/FreeSearchText/FreeSearchText";
import CheckBoxFilter from "./components/CheckboxFilter/CheckboxFilter";
import ProductOverview from "./components/ProductOverview/ProductOverview";
import axios from "axios";

const url =
  "https://pfp-public-productdb-api.azurewebsites.net/api/product/search";

function App() {
  const [productList, setProductList] = useState(null);
  const [loading, setloading] = useState(false);
  const [error, setError] = useState("");
  const [searchText, setsearchText] = useState("");
  const [typechecked, setTypeChecked] = useState(false);
  const [checkboxText, setCheckboxText] = useState("");
  const [typeChecked2, setTypeChecked2] = useState(false);
  const [checkboxText2, setCheckboxText2] = useState("");
  console.log(checkboxText2);
  const onHandleSearchText = (e) => {
    console.log(e);
    setsearchText(e.target.value);
  };

  const productOverview = async () => {
    try {
      setloading(true);
      const { data } = await axios.post(url, {
        productCategoryTypeIds: [1, 2],
      });

      setProductList(data.results);
      setloading(false);
    } catch (error) {
      setError(error);
    }
  };

  const checkBoxFilter = async () => {
    try {
      if (
        checkboxText.toLowerCase().includes("Farlige produkter".toLowerCase())
      ) {
        setloading(true);
        const { data } = await axios.post(url, {
          productCategoryTypeIds: [2],
        });

        setProductList(data.results);
        setloading(false);
      } else {
        setloading(true);
        const { data } = await axios.post(url, {
          productCategoryTypeIds: [1],
        });
        setProductList(data.results);
        setloading(false);
      }
    } catch (error) {
      setError(error);
    }
  };

  useEffect(() => {
    productOverview();
  }, []);

  useEffect(() => {
    checkBoxFilter();
  }, [checkboxText]);

  return (
    <>
      <CssBaseline />
      <NavBar />
      <main>
        <IntroSection />
        <ButtonText />
        <Container maxWidth="lg">
          <Grid container spacing={1}>
            <Grid container item xs={12} md={4}>
              <FreeSearchText
                searchTerm={searchText}
                handleTerm={onHandleSearchText}
              />
              <CheckBoxFilter
                checkedProducttype={typechecked}
                changeTypeChecked={setTypeChecked}
                checkboxTerm={setCheckboxText}
                checkedProducttype2={typeChecked2}
                setCheckedProducttype2={setTypeChecked2}
                setChangeType2Text={setCheckboxText2}
              />
            </Grid>
            <ProductOverview
              products={productList}
              errors={error}
              load={loading}
            />
          </Grid>
        </Container>
      </main>
    </>
  );
}

export default App;
